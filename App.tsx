import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import {ApolloClient, InMemoryCache, ApolloProvider, createHttpLink} from '@apollo/client';

import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';

export const link = createHttpLink({
	uri: "http://it2810-27.idi.ntnu.no:3000/"
	//uri: "http://localhost:4000/"
});

export const client = new ApolloClient({
	cache: new InMemoryCache(),
	link,
});

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

	//const client = new ApolloClient({
		//uri: "http://localhost:4000/",
		//cache: new InMemoryCache(),
	//});

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
			<ApolloProvider client={client}>
				<SafeAreaProvider>
					<Navigation colorScheme={colorScheme} />
					<StatusBar />
				</SafeAreaProvider>
			</ApolloProvider>
    );
  }
}
			//<ApolloProvider client={client}>
			//</ApolloProvider>
