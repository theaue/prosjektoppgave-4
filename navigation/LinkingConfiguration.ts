import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          search: {
            screens: {
              search: 'one',
            },
          },
          TabOne: {
            screens: {
              TabOne: 'two',
            },
          },
          TabTwo: {
            screens: {
              TabTwo: 'three',
            },
          },
        },
      },
      NotFound: '*',
    },
  },
};
