import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import { StyleSheet} from 'react-native';

import Colors from '../constants/Colors';
import { MonoText } from './StyledText';
import { Text, View } from './Themed';

export default function EditScreenInfo({ path }: { path: string }) {
  return (
    <View>
      <View style={styles.container}>
        <Text
          style={styles.getStartedText}
          lightColor="rgba(0,0,0,0.8)"
          darkColor="rgba(255,255,255,0.8)">
          This is a screen:
        </Text>

        <View
          style={[styles.codeHighlightContainer, styles.homeScreenFilename]}
          darkColor="rgba(255,255,255,0.05)"
          lightColor="rgba(0,0,0,0.05)">
          <MonoText>{path}</MonoText>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
		borderStyle: 'dotted',
		borderWidth: 1,
    backgroundColor: '#fff',
  },
  getStartedText: {
    fontSize: 17,
    lineHeight: 24,
    textAlign: 'center',
  },
  codeHighlightContainer: {
		borderStyle: 'dotted',
		borderWidth: 1,
		backgroundColor: '#fff',
    borderRadius: 3,
    paddingHorizontal: 4,
		marginLeft: 10,
		marginRight: 10,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
});
