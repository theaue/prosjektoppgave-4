import * as React from 'react';
import {StyleSheet, TouchableOpacity, Linking, TextInput, Button} from 'react-native';
import {Animated, Dimensions} from 'react-native';
import {PinchGestureHandler, State} from 'react-native-gesture-handler';

import {BottomTab, TabOneNavigator} from '../Navigation/BottomTabNavigator';
import {Text, View} from '../components/Themed';
import Colors from '../constants/Colors';
import EditScreenInfo from '../components/EditScreenInfo';
import Geocoder from '../components/Geocoder';
import Travel from '../components/Travel';
// --------------------------------------------------------------------------------

export default function test(){
	const [value, setValue] = React.useState("value")

	return (
		<View>
			<Button
				onPress={() => setValue("search")}
				title="Click here for a new search"
			/>
			<Text>value: {value}</Text>
		</View>
	)
}
