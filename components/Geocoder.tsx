import React from 'react';
import { gql, useQuery } from "@apollo/client";

import {Text, View} from './Themed';
import {FlatList, Button} from 'react-native';
import Travel from '../components/Travel';
// -----------------------------------------------------------------------------

export const LOCATION_QUERY = gql`
query getLocation($name: String!){
    features(name: $name) {
      properties{
        id
        name
        county
        locality
      }
    }
  }
`;

interface locationData {
    features: features[];
}

interface features {
    properties: properties;
}

interface properties {
    id: string;
    name: string;
    county: string;
    locality: string;
}

interface locationVars {
    name: string;
}

interface Props{
	name: string;
}

export default function Geocoder(props:Props){

	const {loading, data} = useQuery<locationData, locationVars>(
			LOCATION_QUERY,
			{variables: {name: props.name}}
	);

if (loading) return <Text>Loading...</Text>
	return (
		<View>
			{data.features.map((test) =>
				{if(test && test.properties){
					return(
						<Text>
							name: {test.properties.name}{'\n'}
							id: {test.properties.id}{'\n\n'}
						</Text>
					)
				}}
			)}
		</View>
	)
}
