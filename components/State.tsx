import React, (useState) from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';

const State = () => {
	const [count, setCount] = useState(0);

	return (
		<View style={styles.container}>
			<Text>This is some text and a button that you clicked
				{count} times.
			</Text>
			<Button>
				onPress={() => setCount(count + 1)}
			</Button>
		</View>
	)
}

