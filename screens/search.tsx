import * as React from 'react';
import {StyleSheet, TouchableOpacity, Linking, TextInput, Button} from 'react-native';
import {PinchGestureHandler, State} from 'react-native-gesture-handler';

import {BottomTab, TabOneNavigator} from '../Navigation/BottomTabNavigator';
import {Text, View} from '../components/Themed';
import Colors from '../constants/Colors';
import EditScreenInfo from '../components/EditScreenInfo';
import Geocoder from '../components/Geocoder';
import Travel from '../components/Travel';
import Test from '../components/test';
// --------------------------------------------------------------------------------

export default function search({
  navigation,
}: StackScreenProps<RootStackParamList, 'serch'>) {

	const [value, setValue] = React.useState("default value")
	const [count, setCount] = React.useState(0);
	const [state, setState] = React.useState("search");

	// remember to remove this
	const ChangeText = (e) => {
		setValue(e)
		setState("results")
	}

	const getResults = () => {
		if(state === "results")
			return(
				<Geocoder name={value}/>
			)
	}

	if(state == "search")
		return(
			<View style={styles.container}>
				<Text style={styles.title}>Search for something </Text>
				<View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />

					<View style={styles.searchContainer}>
						<View style={styles.box1}><EditScreenInfo path="/screens/search.js" /></View>
						<View style={styles.box2}>
							<Text>Search here to find your next travel</Text>
							<TextInput
								style={{height: 40, width: 200, borderColor: 'gray', borderWidth:1}}
								onEndEditing={(e) => ChangeText(e.nativeEvent.text)}
								placeholder ="search"
							/>
						</View>
					</View>
			</View>
		);
		return(
				<View style={styles.results}>
					<View style={styles.header}>
						<Button
							onPress={() => setState("search")}
							title="Click here for a new search"
						/>
						<Text style={styles.title}> Display search results </Text>
					</View>
					<View style={styles.body}>
						<Geocoder name={value} />
					</View>
				</View>
		);
}
// --------------------------------------------------------------------------------------------
const styles = StyleSheet.create({
  container: {
		flex: 1,
		flexDirection: 'column',
		paddingTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
		borderStyle: 'dotted',
		borderWidth: 1,
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  searchContainer: {
		flex: 1,
		height: 500,
		marginTop: 15,
		marginBottom: 5,
    marginHorizontal: 20,
		width: "90%",
  },
	box1:{
		padding: 10,
		margin: 10,
	},
	box2:{
		padding: 10,
		margin: 10,
	},
	box3:{
		borderStyle: 'dotted',
		borderWidth: 1,
		padding: 10,
		margin: 10,
	},
	results:{
		flex: 1,
		flexDirection: "column",
		justifyContent: 'space-between',
		padding: 10,
		alignItems: 'center',
	},
	header:{
		width: '100%',
		flex: 1,
	},
	body:{
		flex: 1,
	}
});
