Readme

I chose to work on the first alternative and implement a react native client.
The app is a simple app which is based on project 3, and implemented with react-native and
using expo-cli.

The app is tested and is working on Android. Due to the fact that I do not own an iPhone
or a mac, I have unfortunately not been able to test the app on iPhone.

The technologies that I have used in developing this app is react-native, Typescript, the backend is
written with GraphQL and I have also used ApolloClient. For displaying the content on a mobile
device I have used the 'View' and 'Text' elements from 'react-native', and a stack-based navigation
ts used to implement the 'BottomTabNavigator' and to navigate between different screens.
